<?php 
/*
   Plugin Name: video_custom_plg
   Plugin URI: yura.savandor.com
   Description: video_custom_plg
   Version: 1.0
   Author: Yura Marushchak
   Author URI: yura.savandor.com
   License: GPL2
*/

   /**
    * return all categories from current Taxonomy
    */
   class Tax
   {
	   	public static function getTax ($tax = false)
	   	{
	   		if (!$tax)
	   			return false;

	   		return get_terms(
	   			array(
	   				'taxonomy' => array($tax),
	   				'get' => 'all'
	   			)
	   		);
	   	}
   }

   /**
    *  actions with videos
    */
   class Videos 
   {
   		public static function get_post_type($title_post = false, $taxon_title = false, $main_taxon = false)
   		{
   			if (!$title_post || !$taxon_title || !$main_taxon)
   				return false;
   			$Query = new WP_Query(array(
   				"post_type" => $title_post,
   				'orderby'=>'menu_order',
   				'order' => 'ASC',
   				"tax_query" => 
   				[
   					'relation' => 'OR',
   					[
   						'taxonomy' => $main_taxon,
   						'field'=>'slug',
   						'terms' => $taxon_title,
   						'operator' => 'IN',
   					],
   				],
   				'posts_per_page' => -1,
   			));
   				$videos = $Query->get_posts();
   				return $videos;
   		}
   		public static function get_each_video($posts = false, $tax_ = false)
   		{
   			if (!$posts || !$tax_)
   				return false;
   			$data = [];
   			if (!empty($posts)) {
   				foreach ($posts as $post) {
   					$data [] = self::get_Data_each($post->ID, $tax_);
   				}
   				return $data;
   			}

   		}
   		public static function get_Data_each($id = false, $tax_ = false)
   		{
   			if(!$id)
   				return false;
   			$current = get_fields($id);
   			$current = $current['videos'];
   			$data = new \stdClass;

   				$data->id = $id;
   				$data->tax_ = $tax_;
   				$data->title = $current['title'];
   				$data->img_for_video = $current['img_for_video'];
   				$data->link = $current['link'];
   				$data->duration = $current['duration'];
   				$data->diskUrl = $current['diskUrl'];
               $data->notes = $current['notes'];

   			return $data;

   		}

   }
?>