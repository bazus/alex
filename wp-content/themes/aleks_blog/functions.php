<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }

/**
 * Load framework
 */
require_once ( trailingslashit( get_template_directory() ) . '/inc/init.php' );


/**
 * Setup Yellow Pencil Pro
 */
define('YP_THEME_MODE', "true");

function additional_mime_types( $mimes ) {
	$mimes['.rar'] = application/x-zip-compressed;
	$mimes['.zip'] = application/x-zip-compressed;
	

	return $mimes;
}
add_filter( ‘upload_mimes’, ‘additional_mime_types’ );