jQuery(document).ready(function () {
    
    /*Dropdown Menu*/
    if (jQuery('div').is('.dropdown')) {

      jQuery('.tabs-dropdown').click(function () {
          jQuery(this).find('.dropdown').attr('tabindex', 1).focus();
          jQuery(this).find('.dropdown').toggleClass('active');
          jQuery(this).find('.dropdown').find('.dropdown-menu').slideToggle(300);
      });
      jQuery('.dropdown').focusout(function () {
          jQuery(this).removeClass('active');
          // jQuery(this).find('.dropdown-menu').slideUp(300);
      });
      jQuery('.dropdown .dropdown-menu li').click(function () {
          jQuery(this).parents('.dropdown').find('span').text(jQuery(this).text())
          jQuery(this).parents('.dropdown').find('input').attr('value', jQuery(this).attr('id'));
      });

  }
  /*End Dropdown Menu*/


// tabs--------------

jQuery('.section-').addClass('hidden');
jQuery('.section-').first().removeClass('hidden');

jQuery('.tabs-item a, .dropdown-menu li a').on('click', function (e) { 
    e.preventDefault();
    if(jQuery(this).parent().hasClass('tabs-item')){
        jQuery('.tabs-item').removeClass('item-active');
        jQuery(this).parent().addClass('item-active');
    }
    jQuery('.section-').hide();
    jQuery( jQuery(this).attr('href') ).removeClass('hidden');
    jQuery( jQuery(this).attr('href') ).show();
 });
// -----/tabs-------
});



