<?php
/* Template Name: Video page */ 

get_header();
?>
<?php
	$vidos_arr = [];
	
	$taxs = Tax::getTax('video_categories');
if ($taxs){
	foreach ($taxs as $tax) {
		$video = Videos::get_post_type('video_post_type', $tax->slug, $tax->taxonomy);
		$video_s = Videos::get_each_video($video, $tax->name);
		$vidos_arr[$tax->slug]['array'] = $video_s;
		$vidos_arr[$tax->slug]['title'] = $tax->name;	
		$vidos_arr[$tax->slug]['section'] = $tax->slug;	
	
		
	}
	
}
 ?>
<section class="video-section">
  <div class="tabs">
    <div class="container">
    <!-- Десктоп табы -->
    <?php if($taxs) : ?>
      <ul class="tabs-ul">
	      	<?php foreach ($taxs as $key => $tax): ?>
	      		<?php if($tax->count > 0): ?>
		        	<li class="tabs-item <?php echo  $key === 0 ? "item-active" : ""; ?> "><a href="#<?php echo $tax->slug ?>"><?php echo $tax->name ?></a></li>
		   		<?php endif;?>
	    	<?php endforeach;?>
      </ul>
      <!-- Мобайл табы -->
      <div class="tabs-dropdown">
        <div class="dropdown">
          <div class="select">
            <span id="input-format"><?php echo $taxs[0]->name ?></span>
            <i><img src="<?php echo get_template_directory_uri() ?>/img/Down_arrow.png"></i>
          </div>
          <input type="hidden" name="format">
          <ul class="dropdown-menu">
	    	<?php foreach ($taxs as $key => $tax): ?>
	    		<?php if($tax->count > 0): ?>
              		<li id="<?php echo $key ?>"><a href="#<?php echo $tax->slug ?>"><?php echo $tax->name ?></a></li>
              	<?php endif;?>
            <?php endforeach;?>
          </ul>
        <?php endif;?>
      </div>

    </div>
  </div>
  </div>

<?php if($vidos_arr): ?>
	<?php foreach ($vidos_arr as $key => $vidos):?>
	
		<section id="<?php echo $vidos['section']; ?>" class="section-">
		    <div class="container">
		        <h2 class="title"><?php echo $vidos['title']; ?></h2>
		    </div>
		    <div class="container">
		      <div class="video-block">
		        <div class="video-player">
		            <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $vidos['array'][0]->link; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		        </div>
		        <div class="sidebar">
		        	<div class="container download-btn-mobile">
		        		<a  href="<?php echo $vid->notes ?>" download>Скачать</a>

		        	</div>
		            <div class="search" style="width:100%"> 
		                <input class="input-search" type="text" placeholder="Поиск">
		            </div>
		            <div class="recomended-videos">


		      <!-- --------------------------------------- -->

		      	<?php if($vidos['array']): ?>
				      <?php foreach ($vidos['array'] as $key => $vid):?>
				              <div class="rec-video" download_url="<?php echo $vid->diskUrl ?>" you_tube_link="<?php echo $vid->link  ?>">
				                <div class="rec-video__img">
				                  <img src="<?php echo $vid->img_for_video ?>" alt="">
				                  <span class="rec-video__time"><?php echo $vid->duration ?></span>
				                </div>      
				                <span class="rec-video-name"><?php echo $vid->title ?></span>
				              </div>
				        <?php endforeach;?>
		     	<?php endif; ?>
		              <!-- --------------------------------------- -->
		          </div>
		      </div>
		      </div>
		      <a  href="<?php echo $vid->notes ?>" download class="download-btn">Скачать</a>
		    </div>
		</section>
	<?php endforeach;?>
<?php endif;?>
</section>

<?php

get_footer();